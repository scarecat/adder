# Adder

A simple program for inserting and updating html templates on a website.

## Usage

You need to have Python installed in order to run the program.

Download `adder.py` and run it in the terminal with:
```
python ./adder.py
```
Adder goes recursively through the current directory,
looking for files ending with `.html`,
and templates (ending with`.part.html`).

It looks for these patterns in html files:
```html
...
<!-- $partname -->
...
<!-- /$partname -->
...
```
where `$partname` gets translated to `partname.part.html`, 
which gets inserted in between the comments.

# Flags
- `--dry-run`
    Prevents any writes to any files, for checking purposes.
- `--watch`
    Continuously watches for file changes for live updates (very useful combined with `live-server`).
    Keep in mind it does *not* check for added files, only edited and removed files.
 

