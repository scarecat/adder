#!/usr/bin/env python

from pathlib import Path
import re
import sys
from time import sleep

class MismatchedTagError(ValueError):
    def __init__(self, group_1: str, group_2: str):
        self.message = f"Mismatched tag: {group_1} and {group_2}"
        super().__init__(self.message)

adder_pattern = re.compile(r"<!-- +\$([^ ]+) +-->(.*?)<!-- +\/\$([^ ]+) +-->", flags=re.DOTALL)
parts: dict[str, Path] = dict()

dry_run: bool = "--dry-run" in sys.argv
watch_mode: bool = "--watch" in sys.argv


# purely for tracking part changes (todo: make it not global ;-;)
current_parts: list[str] = []

def on_reg_sub(match: re.Match[str]) -> str:
    if match.group(1) != match.group(3):
        raise MismatchedTagError(match.group(1), match.group(3))
    part_name: str = match.group(1)
    part_filename = parts.get(part_name)
    if part_filename is None:
        print(f"Part not found: {part_name}", file=sys.stderr)
        return match.string
    else:
        current_parts.append(part_name)
        part_text: str
        with open(part_filename, "r") as part_file:
            print(f"  part: {part_name} -> {part_filename}")
            part_text = part_file.read()
        return f"<!-- ${part_name} -->\n{part_text}<!-- /${part_name} -->"

def parse(file_path: Path) -> None:
    original_text: str = ""
    text: str = ""
    newlines: str | tuple[str, ...] | None

    current_parts.clear()
    with open(file_path, "r") as file:
        text = file.read()
        original_text = text
        newlines = file.newlines
    try:
        text = re.sub(adder_pattern, on_reg_sub, text)
    except MismatchedTagError as e:
        print(e.message, file=sys.stderr)
        return

    if len(current_parts) == 0:
        print("No parts in file.")
        return
    if text == original_text:
        print("Nothing to change.")
    else:
        if dry_run:
            print("Not applying changes (dry run).")
        else:
            with open(file_path, "w+", newline=str(newlines)) as output_file:
                output_file.seek(0)
                output_file.write(text)
                print("Applied changes.")

def run(watch_map = None) -> None:
    path = Path(".")

    part_files = list(path.glob("**/*.part.html"))
    page_files = [f for f in path.glob("**/*.html") if not f in part_files]

    if watch_map is not None: 
        for file in part_files + page_files:
            watch_map[file] = file.stat().st_mtime

    for part_path in part_files:
        part_name = part_path.stem.split(".")[0]
        print(f"Found part: {part_name} -> {part_path}")
        if parts.get(part_name) is None:
            parts[part_name] = part_path
        else:
            print(f"Part {part_name} already exists, skipping...")
    
    for page_path in page_files:
        print(f"{page_path}")
        parse(page_path)
        print("")


def main() -> None:
    if watch_mode:
        watch_map: dict[Path, float] = {}
        run(watch_map)
        try:
            while True:
                try:
                    for (path, mtime) in watch_map.items():
                        if path.stat().st_mtime > mtime:
                            sleep(0.25)
                            run(watch_map)
                            break
                except FileNotFoundError:
                        sleep(0.25)
                        run(watch_map)
                        
                sleep(0.25)
        except KeyboardInterrupt:
            return
    else:
        run()

if __name__ == "__main__":
    main()
